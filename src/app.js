const MAX_32bits_VALUE = Math.pow(2, 32);

const date = new Date(MAX_32bits_VALUE * 1000);
const months = [ "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre" ];
console.log(date);
console.log(`Le bug arrivera le ${ date.getDate() } ${ months[date.getMonth()] } ${ date.getFullYear() }`);
